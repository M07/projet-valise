package projetvalise

import projetvalise.domain.MediaController


class EncoderService {

    def encodeVideo(ArrayList cmd, def grailsApplication, long id, String oldPath, String resultPath, MediaController controller) {

        Thread.start {
            println("Encoder service begin process")
            Process executingProcess = cmd.execute()

            //redirect program outputs to the console
            def errorStreamPrinter = new StreamPrinter(executingProcess.getErrorStream())
            def outputStreamPrinter = new StreamPrinter(executingProcess.getInputStream())
            [errorStreamPrinter, outputStreamPrinter]*.start()

            //wait the program termination
            executingProcess.waitFor()

            //if the result file is created, the temporary file is removed else, the content object is removed
            def resFile = grailsApplication.parentContext.getResource(resultPath)
            if (resFile.file?.exists()) {
                println("result file exists")

                //temporary file is removed
                def tempFile = grailsApplication.parentContext.getResource(oldPath)
                if (tempFile.file?.exists()) {
                    println("delete : " + oldPath)
                    tempFile.file.delete()
                }
                println("Encoder service update filename with : " + resultPath)
                Media.withTransaction {
                    controller.updateMediaFilename(id, resultPath)
                }
            }else{
                println("result file does not exists")
                controller.delete(id)
            }
            println('Encoder service Process finished');
        }
    }


}

class StreamPrinter extends Thread {
    InputStream inputStream

    StreamPrinter(InputStream is) {
        this.inputStream = is
    }

    public void run() {
        new BufferedReader(new InputStreamReader(inputStream)).withReader {reader ->
            String line
            while ((line = reader.readLine()) != null) {
                println(line)
            }
        }
    }
}

