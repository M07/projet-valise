
/**
 * Created with IntelliJ IDEA.
 * User: M07
 * Date: 26/01/13
 * Time: 15:10
 * To change this template use File | Settings | File Templates.
 */
import java.security.MessageDigest
class SHACodec{
	static encode = {target->
		MessageDigest md = MessageDigest.getInstance('SHA')
		md.update(target.getBytes('UTF-8'))
		return new String(md.digest()).encodeAsBase64()
	}
}