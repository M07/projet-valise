package projetvalise

abstract class Content {
	Date dateCreated;
	Date lastUpdated;
	String title;
	String description;

	static constraints = {
		dateCreated()
		lastUpdated()
		title(nullable: false, blank: false)
		description(nullable: false, blank: false)
	}
}
