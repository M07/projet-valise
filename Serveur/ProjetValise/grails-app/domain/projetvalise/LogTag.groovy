package projetvalise
import projetvalise.Tag.TagType
class LogTag {

	Date dateCreated
	Double latitude
	Double longitude
	Tag tag
	TagType typeTag
	static constraints = {
		dateCreated()
		tag(nullable: false)
		typeTag(blank: false)
		latitude(min: -90d, max: 90d)
		longitude(min: -180d, max: 180d)
	}
}
