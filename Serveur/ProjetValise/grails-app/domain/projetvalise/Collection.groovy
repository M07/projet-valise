package projetvalise

class Collection {
	User owner
	Double latitude
	Double longitude
	static hasMany = [tags: Tag]
	static mappedBy = [collection: Collection]
	static constraints = {
		owner(nullable: false)
		latitude(min: -90d, max: 90d)
		longitude(min: -180d, max: 180d)
	}
}
