package projetvalise

class Media extends Content {
	enum TypeFile {
		VIDEO,AUDIO,IMAGE
	}
	String fileName
	TypeFile type
	byte[] dataFile
	static constraints = {
		fileName(nullable: false)
		type(nullable: false)
	}
	static transients = ['dataFile']
}
