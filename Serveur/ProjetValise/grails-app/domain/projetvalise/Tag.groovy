package projetvalise

class Tag {
	enum TagType {
		NFC, QR, SNS
	}
	String tag
	TagType type
	Date dateCreated
	Content content
    Collection collection
	static hasMany = [logTags: LogTag];

    static constraints = {
		tag(nullable: false, blank: false, unique: true)
		dateCreated()
		content( nullable: true )
		type(nullable: false)
        collection(nullable: true)
    }
}
