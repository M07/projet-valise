package projetvalise

class User {
	static constraints = {
		login(blank:false, nullable:false, unique:true)
		password(blank:false, password:true)
		dateCreated()
		role(blank: false, inList:["admin", "user"])
	}

	String login
	String password
	Date dateCreated
	String role = "user"
	static hasMany = [collections: Collection];

	String toString(){
		login
	}

	boolean isAdmin(){
		return role == "admin"
	}

	def beforeInsert = {
		password = password.encodeAsSHA()
	}
}