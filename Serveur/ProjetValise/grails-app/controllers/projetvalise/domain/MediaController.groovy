package projetvalise.domain

import projetvalise.Media
import projetvalise.Media.TypeFile
import org.apache.commons.lang.SystemUtils
import org.springframework.dao.DataIntegrityViolationException
import projetvalise.EncoderService

class MediaController {
    def scaffold = Media

    def mediaType = {
		TypeFile type
		if(it.toString().contains("audio"))  {
			type = TypeFile.AUDIO
		}
		if(it.toString().contains("image"))  {
			type = TypeFile.IMAGE
		}
		if(it.toString().contains("video"))  {
			type = TypeFile.VIDEO
		}
		type
	}

    def encoderService

	def resDeletes(resPath)  {
		def resFile = grailsApplication.parentContext.getResource(resPath)
		if (resFile.file?.exists()) {
			return resFile.file.delete()
		}
		return false
	}

	def save() {
		def f = request.getFile('uploadedFile')
        String encodedVideoExtension = '.mp4'
        String encodedAudioExtension = '.mp3'
        String mediaRoot = 'medias/'

        if (f.empty) {
			flash.message = 'DataFile cannot be empty !'
			render(view: "create")
			return
		}

        TypeFile type = mediaType(f.getContentType())
        String originalFilename = f.getOriginalFilename()
        String title = originalFilename.substring(0,originalFilename.lastIndexOf('.'))
        String extension = originalFilename.substring(originalFilename.lastIndexOf('.'))
        String tempExtension = '.temp'

        String filename = new Date().toTimestamp().time

        params.put("fileName", mediaRoot+filename+extension)//We add the fileName property (required) to create a new media
        params.put("type", type)//We add the type property (required) to create a new media
        def mediaInstance = new Media(params)//We create a new media
        mediaInstance.title = title
        if (!mediaInstance.save(flush: true)) {//We save the media
            render(view: "create", model: [mediaInstance: mediaInstance])//We stay on the current view if validations are failed
            return
        }

        new File('web-app/'+mediaRoot).mkdirs()

        def cmd;
        //execute the conversion program
        // -ar[:stream_specifier] freq (input/output,per-stream) : Set the audio sampling frequency.
        //      For output streams it is set by default to the frequency of the corresponding input stream.
        //      For input streams this option only makes sense for audio grabbing devices and raw demuxers
        //      and is mapped to the corresponding demuxer options.
        // -y : Overwrite output files without asking.
        // -n : Do not overwrite output files but exit if dataFile exists.
        // -i : input dataFile name.

        switch(type){
            case TypeFile.VIDEO :
                f.transferTo(new File('web-app/'+mediaRoot+filename+tempExtension))//We create physical dataFile with the name of the uploaded dataFile
                if(SystemUtils.IS_OS_MAC){
                    cmd = ['ffmpeg/OSX/ffmpeg', '-i', 'web-app/'+mediaRoot+filename+tempExtension, '-ar', '48000', '-y', 'web-app/'+mediaRoot+filename+encodedVideoExtension]
                }else if (SystemUtils.IS_OS_WINDOWS){
                    cmd = ['ffmpeg/win/ffmpeg.exe', '-i', 'web-app/'+mediaRoot+filename+tempExtension, '-ar', '48000', '-y', 'web-app/'+mediaRoot+filename+encodedVideoExtension]
                }
                encoderService.encodeVideo(cmd, grailsApplication, mediaInstance.id, mediaRoot+filename+tempExtension, mediaRoot+filename+encodedVideoExtension, this)

                break
            case TypeFile.AUDIO :
                f.transferTo(new File('web-app/'+mediaRoot+filename+tempExtension))//We create physical dataFile with the name of the uploaded dataFile
                if(SystemUtils.IS_OS_MAC){
                    cmd = ['ffmpeg/OSX/ffmpeg', '-i', 'web-app/'+mediaRoot+filename+tempExtension, '-b:a', '128k', '-y', 'web-app/'+mediaRoot+filename+encodedAudioExtension]
                }else if (SystemUtils.IS_OS_WINDOWS){
                    cmd = ['ffmpeg/win/ffmpeg.exe', '-i', 'web-app/'+mediaRoot+filename+tempExtension, '-b:a', '128k',
                            '-bufsize', '3M', '-ac', '2',  '-acodec', 'libmp3lame', '-y', 'web-app/'+mediaRoot+filename+encodedAudioExtension]
                }
                encoderService.encodeVideo(cmd, grailsApplication, mediaInstance.id, mediaRoot+filename+tempExtension, mediaRoot+filename+encodedAudioExtension, this)

                break
            default:
                f.transferTo(new File('web-app/'+mediaRoot+filename+extension))//We create physical dataFile with the name of the uploaded dataFile
                break
        }


        flash.message = message(code: 'default.created.message', args: [message(code: 'media.label', default: 'Media'), mediaInstance.id])
        redirect(action: "show", id: mediaInstance.id)

	}

	def delete(Long id) {
		def mediaInstance = Media.get(id)
		if (!mediaInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'media.label', default: 'Media'), id])
			redirect(action: "list")
			return
		}
		String path = mediaInstance.fileName;
		try {
			mediaInstance.delete(flush: true)
			if (resDeletes(path)) {
				flash.message = message(code: 'default.deleted.message', args: [message(code: 'media.label', default: 'Media'), id])
			} else {
				flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'media.label', default: 'Media'), id])
			}
			redirect(action: "list")
		}
		catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'media.label', default: 'Media'), id])
			redirect(action: "show", id: id)
		}
	}

    def updateMediaFilename(def id, def filename){
        Media media = Media.get(id)
        media.setFileName(filename)
        println("Trying to save media " + media.getId())
        media.save(flush: true)
        println("Filename for media " + media.getId()+ " updated.")
    }

}
