package projetvalise.domain

import projetvalise.User

class UserController {
	def scaffold = User

	def login = {}

	def authenticate = {
		def user = User.findByLoginAndPassword(params.login, params.password.encodeAsSHA())
		if(user){
			session.user = user
			flash.message = "Hello ${user.login}!"
			redirect(url: "/")
		}else{
			flash.message = "Sorry, ${params.login}. Please try again."
			redirect(action:"login")
		}
	}

	def logout = {
		flash.message = "Goodbye ${session.user.login}"
		session.user = null
		redirect(url: "/")
	}

	/*def beforeInterceptor = [action:this.&auth,
			except:['login', 'logout', 'authenticate']]*/
	def auth() {
		if(!session.user) {
			redirect(controller:"user", action:"login")
			return false
		}
	}
}