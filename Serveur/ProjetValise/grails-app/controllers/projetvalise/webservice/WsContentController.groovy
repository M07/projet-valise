package projetvalise.webservice

import grails.converters.JSON
import projetvalise.Content
import projetvalise.Tag
import projetvalise.Smell
import projetvalise.LogTag
import projetvalise.Collection

class WsContentController {

    def show = {
        if(params.id){
            Tag tag = Tag.findByTag(params.id)
			if(tag) {
				Content content = tag.content
				if(content instanceof Smell){
					def smell = [
							dateCreated:content.dateCreated,
							lastUpdated:content.lastUpdated,
							description:content.description,
							title:content.title,
							smell:true
					]
					render smell as JSON
				}else{
					render Tag.findByTag(params.id)?.content as JSON
				}

                LogTag logTag = new LogTag()
                logTag.setTag(tag)
                logTag.setTypeTag(tag.getType())
                logTag.setLongitude(tag.getCollection()?.getLongitude())
                logTag.setLatitude(tag.getCollection()?.getLatitude())
                if (!logTag.save(flush: true)) {
					println logTag?.errors
                    return
                }
			} else {
				def errorJson = [
					error:"Tag " + params.id + " not found"
				]
				render errorJson as JSON
			}
        }
    }

    def save = {}
    def update = {}
    def remove = {}
}
