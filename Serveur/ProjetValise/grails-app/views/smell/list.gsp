
<%@ page import="projetvalise.Smell" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'smell.label', default: 'Smell')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-smell" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-smell" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'smell.dateCreated.label', default: 'Date Created')}" />
					
						<g:sortableColumn property="lastUpdated" title="${message(code: 'smell.lastUpdated.label', default: 'Last Updated')}" />
					
						<g:sortableColumn property="title" title="${message(code: 'smell.title.label', default: 'Title')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'smell.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="data" title="${message(code: 'smell.data.label', default: 'Data')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${smellInstanceList}" status="i" var="smellInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${smellInstance.id}">${fieldValue(bean: smellInstance, field: "dateCreated")}</g:link></td>
					
						<td><g:formatDate date="${smellInstance.lastUpdated}" /></td>
					
						<td>${fieldValue(bean: smellInstance, field: "title")}</td>
					
						<td>${fieldValue(bean: smellInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: smellInstance, field: "data")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${smellInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
