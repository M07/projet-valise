<%@ page import="projetvalise.Smell" %>



<div class="fieldcontain ${hasErrors(bean: smellInstance, field: 'title', 'error')} required">
	<label for="title">
		<g:message code="smell.title.label" default="Title" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="title" required="" value="${smellInstance?.title}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: smellInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="smell.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${smellInstance?.description}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: smellInstance, field: 'data', 'error')} required">
	<label for="data">
		<g:message code="smell.data.label" default="Data" />
		<span class="required-indicator">*</span>
	</label>
	<input type="file" id="data" name="data" />
</div>

