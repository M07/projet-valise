<%@ page import="projetvalise.Media" %>


<div class="fieldcontain ${hasErrors(bean: mediaInstance, field: 'dataFile', 'error')} required">
    <label for="file">
        <g:message code="media.file.label" default="File" />
        <span class="required-indicator">*</span>
    </label>
    <input type="file" id="file" name="uploadedFile" />
</div>

<div class="fieldcontain ${hasErrors(bean: mediaInstance, field: 'description', 'error')} required">
    <label for="description">
        <g:message code="media.description.label" default="Description" />
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="description" required="" value="${mediaInstance?.description}"/>
</div>
