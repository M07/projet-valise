<%@ page import="projetvalise.Media" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'media.label', default: 'Media')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-media" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                            default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-media" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list media">

        <g:if test="${mediaInstance?.dateCreated}">
            <li class="fieldcontain">
                <span id="dateCreated-label" class="property-label"><g:message code="media.dateCreated.label"
                                                                               default="Date Created"/></span>

                <span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate
                        date="${mediaInstance?.dateCreated}"/></span>

            </li>
        </g:if>

        <g:if test="${mediaInstance?.lastUpdated}">
            <li class="fieldcontain">
                <span id="lastUpdated-label" class="property-label"><g:message code="media.lastUpdated.label"
                                                                               default="Last Updated"/></span>

                <span class="property-value" aria-labelledby="lastUpdated-label"><g:formatDate
                        date="${mediaInstance?.lastUpdated}"/></span>

            </li>
        </g:if>

        <g:if test="${mediaInstance?.title}">
            <li class="fieldcontain">
                <span id="title-label" class="property-label"><g:message code="media.title.label"
                                                                         default="Title"/></span>

                <span class="property-value" aria-labelledby="title-label"><g:fieldValue bean="${mediaInstance}"
                                                                                         field="title"/></span>

            </li>
        </g:if>

        <g:if test="${mediaInstance?.description}">
            <li class="fieldcontain">
                <span id="description-label" class="property-label"><g:message code="media.description.label"
                                                                               default="Description"/></span>

                <span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${mediaInstance}"
                                                                                               field="description"/></span>

            </li>
        </g:if>

        <g:if test="${mediaInstance?.fileName}">
            <li class="fieldcontain">
                <span id="fileName-label" class="property-label"><g:message code="media.fileName.label"
                                                                            default="File Name"/></span>

                <span class="property-value" aria-labelledby="fileName-label"><g:fieldValue bean="${mediaInstance}"
                                                                                            field="fileName"/></span>

            </li>
        </g:if>

        <g:if test="${mediaInstance?.type}">
            <li class="fieldcontain">
                <span id="type-label" class="property-label"><g:message code="media.type.label" default="Type"/></span>

                <span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${mediaInstance}"
                                                                                        field="type"/></span>

            </li>
        </g:if>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${mediaInstance?.id}"/>
            <g:link class="edit" action="edit" id="${mediaInstance?.id}"><g:message code="default.button.edit.label"
                                                                                    default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
