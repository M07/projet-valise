
<%@ page import="projetvalise.LogTag" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'logTag.label', default: 'LogTag')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-logTag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-logTag" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="dateCreated" title="${message(code: 'logTag.dateCreated.label', default: 'Date Created')}" />
					
						<th><g:message code="logTag.tag.label" default="Tag" /></th>
					
						<g:sortableColumn property="typeTag" title="${message(code: 'logTag.typeTag.label', default: 'Type Tag')}" />
					
						<g:sortableColumn property="latitude" title="${message(code: 'logTag.latitude.label', default: 'Latitude')}" />
					
						<g:sortableColumn property="longitude" title="${message(code: 'logTag.longitude.label', default: 'Longitude')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${logTagInstanceList}" status="i" var="logTagInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${logTagInstance.id}">${fieldValue(bean: logTagInstance, field: "dateCreated")}</g:link></td>
					
						<td>${fieldValue(bean: logTagInstance, field: "tag")}</td>
					
						<td>${fieldValue(bean: logTagInstance, field: "typeTag")}</td>
					
						<td>${logTagInstance.latitude}</td>
					
						<td>${logTagInstance.longitude}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${logTagInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
