
<%@ page import="projetvalise.LogTag" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'logTag.label', default: 'LogTag')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-logTag" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-logTag" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list logTag">
			
				<g:if test="${logTagInstance?.dateCreated}">
				<li class="fieldcontain">
					<span id="dateCreated-label" class="property-label"><g:message code="logTag.dateCreated.label" default="Date Created" /></span>
					
						<span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate date="${logTagInstance?.dateCreated}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${logTagInstance?.tag}">
				<li class="fieldcontain">
					<span id="tag-label" class="property-label"><g:message code="logTag.tag.label" default="Tag" /></span>
					
						<span class="property-value" aria-labelledby="tag-label"><g:link controller="tag" action="show" id="${logTagInstance?.tag?.id}">${logTagInstance?.tag?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${logTagInstance?.typeTag}">
				<li class="fieldcontain">
					<span id="typeTag-label" class="property-label"><g:message code="logTag.typeTag.label" default="Type Tag" /></span>
					
						<span class="property-value" aria-labelledby="typeTag-label"><g:fieldValue bean="${logTagInstance}" field="typeTag"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${logTagInstance?.latitude}">
				<li class="fieldcontain">
					<span id="latitude-label" class="property-label"><g:message code="logTag.latitude.label" default="Latitude" /></span>
					
						<span class="property-value" aria-labelledby="latitude-label">${logTagInstance.latitude}</span>
					
				</li>
				</g:if>
			
				<g:if test="${logTagInstance?.longitude}">
				<li class="fieldcontain">
					<span id="longitude-label" class="property-label"><g:message code="logTag.longitude.label" default="Longitude" /></span>
					
						<span class="property-value" aria-labelledby="longitude-label">${logTagInstance.longitude}</span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${logTagInstance?.id}" />
					<g:link class="edit" action="edit" id="${logTagInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
