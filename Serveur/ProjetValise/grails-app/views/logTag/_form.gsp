<%@ page import="projetvalise.LogTag" %>



<div class="fieldcontain ${hasErrors(bean: logTagInstance, field: 'tag', 'error')} required">
	<label for="tag">
		<g:message code="logTag.tag.label" default="Tag" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="tag" name="tag.id" from="${projetvalise.Tag.list()}" optionKey="id" required="" value="${logTagInstance?.tag?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logTagInstance, field: 'typeTag', 'error')} required">
	<label for="typeTag">
		<g:message code="logTag.typeTag.label" default="Type Tag" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="typeTag" from="${projetvalise.Tag$TagType?.values()}" keys="${projetvalise.Tag$TagType.values()*.name()}" required="" value="${logTagInstance?.typeTag?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logTagInstance, field: 'latitude', 'error')} required">
	<label for="latitude">
		<g:message code="logTag.latitude.label" default="Latitude" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="latitude" value="${logTagInstance.latitude}" required="" type="any"/>
</div>

<div class="fieldcontain ${hasErrors(bean: logTagInstance, field: 'longitude', 'error')} required">
	<label for="longitude">
		<g:message code="logTag.longitude.label" default="Longitude" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="longitude" value="${logTagInstance.longitude}" required="" type="any"/>
</div>

