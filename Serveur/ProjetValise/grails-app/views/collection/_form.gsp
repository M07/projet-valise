<%@ page import="projetvalise.Collection" %>



<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'owner', 'error')} required">
	<label for="owner">
		<g:message code="collection.owner.label" default="Owner" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="owner" name="owner.id" from="${projetvalise.User.list()}" optionKey="id" required="" value="${collectionInstance?.owner?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'latitude', 'error')} required">
	<label for="latitude">
		<g:message code="collection.latitude.label" default="Latitude" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="latitude" value="${collectionInstance.latitude}" required="" type="any"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'longitude', 'error')} required">
	<label for="longitude">
		<g:message code="collection.longitude.label" default="Longitude" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="longitude" value="${collectionInstance.longitude}" required="" type="any"/>
</div>

<div class="fieldcontain ${hasErrors(bean: collectionInstance, field: 'tags', 'error')} ">
	<label for="tags">
		<g:message code="collection.tags.label" default="Tags" />
		
	</label>
	<g:select name="tags" from="${projetvalise.Tag.list()}" multiple="multiple" optionKey="id" size="5" value="${collectionInstance?.tags*.id}" class="many-to-many"/>
</div>

