<%@ page import="projetvalise.Content" %>


<div class="fieldcontain ${hasErrors(bean: contentInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="content.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${contentInstance?.description}"/>
</div>

