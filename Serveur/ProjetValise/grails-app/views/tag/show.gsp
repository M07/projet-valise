<%@ page import="projetvalise.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}"/>
    <title><g:message code="default.show.label" args="[entityName]"/></title>
</head>

<body>
<a href="#show-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                          default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]"/></g:link></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="show-tag" class="content scaffold-show" role="main">
    <h1><g:message code="default.show.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <ol class="property-list tag">

        <g:if test="${tagInstance?.tag}">
            <li class="fieldcontain">
                <span id="tag-label" class="property-label"><g:message code="tag.tag.label" default="Tag"/></span>

                <span class="property-value" aria-labelledby="tag-label"><g:fieldValue bean="${tagInstance}"
                                                                                       field="tag"/></span>

            </li>
        </g:if>

        <g:if test="${tagInstance?.dateCreated}">
            <li class="fieldcontain">
                <span id="dateCreated-label" class="property-label"><g:message code="tag.dateCreated.label"
                                                                               default="Date Created"/></span>

                <span class="property-value" aria-labelledby="dateCreated-label"><g:formatDate
                        date="${tagInstance?.dateCreated}"/></span>

            </li>
        </g:if>

        <g:if test="${tagInstance?.content}">
            <li class="fieldcontain">
                <span id="content-label" class="property-label"><g:message code="tag.content.label"
                                                                           default="Content"/></span>

                <span class="property-value" aria-labelledby="content-label"><g:link controller="content" action="show"
                                                                                     id="${tagInstance?.content?.id}">${tagInstance?.content?.encodeAsHTML()}</g:link></span>

            </li>
        </g:if>

        <g:if test="${tagInstance?.type}">
            <li class="fieldcontain">
                <span id="type-label" class="property-label"><g:message code="tag.type.label" default="Type"/></span>

                <span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${tagInstance}"
                                                                                        field="type"/></span>

            </li>
        </g:if>

        <g:if test="${tagInstance?.logTags}">
            <li class="fieldcontain">
                <span id="logTags-label" class="property-label"><g:message code="tag.logTags.label"
                                                                           default="Log Tags"/></span>

                <g:each in="${tagInstance.logTags}" var="l">
                    <span class="property-value" aria-labelledby="logTags-label"><g:link controller="logTag"
                                                                                         action="show"
                                                                                         id="${l.id}">${l?.encodeAsHTML()}</g:link></span>
                </g:each>

            </li>
        </g:if>

    </ol>
    <g:form>
        <fieldset class="buttons">
            <g:hiddenField name="id" value="${tagInstance?.id}"/>
            <g:link class="edit" action="edit" id="${tagInstance?.id}"><g:message code="default.button.edit.label"
                                                                                  default="Edit"/></g:link>
            <g:actionSubmit class="delete" action="delete"
                            value="${message(code: 'default.button.delete.label', default: 'Delete')}"
                            onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
        </fieldset>
    </g:form>
</div>
</body>
</html>
