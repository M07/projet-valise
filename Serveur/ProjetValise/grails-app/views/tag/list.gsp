<%@ page import="projetvalise.Tag" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="${message(code: 'tag.label', default: 'Tag')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
</head>

<body>
<a href="#list-tag" class="skip" tabindex="-1"><g:message code="default.link.skip.label"
                                                          default="Skip to content&hellip;"/></a>

<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
        <li><g:link class="create" action="create"><g:message code="default.new.label"
                                                              args="[entityName]"/></g:link></li>
    </ul>
</div>

<div id="list-tag" class="content scaffold-list" role="main">
    <h1><g:message code="default.list.label" args="[entityName]"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="tag" title="${message(code: 'tag.tag.label', default: 'Tag')}"/>

            <g:sortableColumn property="dateCreated"
                              title="${message(code: 'tag.dateCreated.label', default: 'Date Created')}"/>

            <th><g:message code="tag.content.label" default="Content"/></th>

            <g:sortableColumn property="type" title="${message(code: 'tag.type.label', default: 'Type')}"/>

            <g:sortableColumn property="collection" title="${message(code: 'tag.collection.label', default: 'Collection')}"/>

        </tr>
        </thead>
        <tbody>
        <g:each in="${tagInstanceList}" status="i" var="tagInstance">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show"
                            id="${tagInstance.id}">${fieldValue(bean: tagInstance, field: "tag")}</g:link></td>

                <td><g:formatDate date="${tagInstance.dateCreated}"/></td>

                <td>${fieldValue(bean: tagInstance, field: "content")}</td>

                <td>${fieldValue(bean: tagInstance, field: "type")}</td>

                <td>${fieldValue(bean: tagInstance, field: "collection")}</td>

            </tr>
        </g:each>
        </tbody>
    </table>

    <div class="pagination">
        <g:paginate total="${tagInstanceTotal}"/>
    </div>
</div>
</body>
</html>
