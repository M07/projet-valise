<%@ page import="projetvalise.Tag" %>



<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'tag', 'error')} required">
	<label for="tag">
		<g:message code="tag.tag.label" default="Tag" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="tag" required="" value="${tagInstance?.tag}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'content', 'error')} ">
	<label for="content">
		<g:message code="tag.content.label" default="Content" />
		
	</label>
	<g:select id="content" name="content.id" from="${projetvalise.Content.list()}" optionKey="id" value="${tagInstance?.content?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'type', 'error')} required">
	<label for="type">
		<g:message code="tag.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
	<g:select name="type" from="${projetvalise.Tag$TagType?.values()}" keys="${projetvalise.Tag$TagType.values()*.name()}" required="" value="${tagInstance?.type?.name()}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'collection', 'error')} ">
    <label for="collection">
        <g:message code="tag.collection.label" default="Collection" />

    </label>
    <g:select id="collection" name="collection.id" from="${projetvalise.Collection.list()}" optionKey="id" value="${tagInstance?.collection?.id}" class="many-to-one" noSelection="['null': '']"/>
</div>

<div class="fieldcontain ${hasErrors(bean: tagInstance, field: 'logTags', 'error')} ">
	<label for="logTags">
		<g:message code="tag.logTags.label" default="Log Tags" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${tagInstance?.logTags?}" var="l">
    <li><g:link controller="logTag" action="show" id="${l.id}">${l?.encodeAsHTML()}</g:link></li>
</g:each>
</ul>

</div>

