<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to Grails</title>
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'pretty_menu.css')}" type="text/css">
        <link rel="stylesheet" href="${resource(dir: 'css', file: 'index.css')}" type="text/css">
	</head>
	<body>
		<a href="#page-body" class="skip"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div id='cssmenu'>
            <ul>
                <li class='active'><g:link controller="user" action="list"><span>Users</span></g:link></li>
                <li class='has-sub'><a href="#"><span>Tags</span></a>
                    <ul>
                        <li><g:link controller="tag" action="list"><span>Tags list</span></g:link></li>
                        <li><g:link controller="logTag" action="list"><span>Logs list</span></g:link></li>
                    </ul>
                </li>
                <li class='has-sub'><a href="#"><span>Contents</span></a>
                    <ul>
                        <li><g:link controller="content" action="list"><span>Contents list</span></g:link></li>
                        <li><g:link controller="media" action="list"><span>Media list</span></g:link></li>
                        <li><g:link controller="smell" action="list"><span>Smells list</span></g:link></li>
                    </ul>
                </li>
                <li class='last'><g:link controller="collection" action="list"><span>Collection</span></g:link></li>
            </ul>
        </div>
		<div id="page-body" role="main">
			<h1>Bienvenue sur la plateforme de la valise mémorielle</h1>
			<p>En tant qu'admin vous pourrez effectuer le CRUD sur toutes les tables du modèle. Vous devez donc être connecté pour utiliser cette interface.</p>
            <p>Vous pourrez naviguer sur ce site à l'aide du menu de gauche. Vous accèderez aux différents controlleurs qui sont actuellement déployés dans cette application.</p>
            <p>Cliquez sur chacun d'eux pour exécuter l'action par défaut.</p>
		</div>
	</body>
</html>
