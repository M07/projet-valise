<%@ page import="projetvalise.User" %>



<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'login', 'error')} required">
    <label for="login">
        <g:message code="user.login.label" default="Login"/>
        <span class="required-indicator">*</span>
    </label>
    <g:textField name="login" required="" value="${userInstance?.login}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'password', 'error')} required">
    <label for="password">
        <g:message code="user.password.label" default="Password"/>
        <span class="required-indicator">*</span>
    </label>
    <g:field type="password" name="password" required="" value="${userInstance?.password}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'role', 'error')} required">
    <label for="role">
        <g:message code="user.role.label" default="Role"/>
        <span class="required-indicator">*</span>
    </label>
    <g:select name="role" from="${userInstance.constraints.role.inList}" required="" value="${userInstance?.role}"
              valueMessagePrefix="user.role"/>
</div>

<div class="fieldcontain ${hasErrors(bean: userInstance, field: 'collections', 'error')} ">
    <label for="collections">
        <g:message code="user.collections.label" default="Collections"/>

    </label>

    <ul class="one-to-many">
        <g:each in="${userInstance?.collections ?}" var="c">
            <li><g:link controller="collection" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
        </g:each>
        <li class="add">
            <g:link controller="collection" action="create"
                    params="['user.id': userInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'collection.label', default: 'Collection')])}</g:link>
        </li>
    </ul>

</div>

