package projetvalise

class AdminFilters {
	def filters = {
		adminOnly(controller:'*',
				action:"(create|edit|update|delete|save|list)") {
			before = {
				if(!session?.user?.admin){
					flash.message = "Sorry, admin only"
					redirect(controller: "user", action: "login")
					return false
				}
			}
		}
	}
}