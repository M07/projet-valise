import grails.util.GrailsUtil
import projetvalise.User
import projetvalise.Tag
import projetvalise.Media;

class BootStrap {
	def init = { servletContext ->
		switch(GrailsUtil.environment){
			case "development":
				def admin = new User(login:"admin", password:"pass", role:"admin")
				admin.save()
				if(admin.hasErrors()){
					println admin.errors
				}
			break;
		}
	}
	def destroy = {

	}
}