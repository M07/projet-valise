package com.mbds.valise;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.mbds.valise.bean.Content;
import com.mbds.valise.helpers.DateParserHelper;

/* 
 * This activity allows users to see the content description of a tag previously tagged
 * A button exists to launch this content( AUDIO, IMAGE, VIDEO, SMELL)
 * 
 */
public class ContentDetailActivity extends Activity {
	private Content content;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_detail);
        content = (Content) getIntent().getSerializableExtra("Content");
        
        //We initialize text views
        ((TextView)findViewById(R.id.valueTitle)).setText(content.getTitle());
        ((TextView)findViewById(R.id.valueDateCreated)).setText(DateParserHelper.parseDateToString(content.getDateCreated()));
        ((TextView)findViewById(R.id.valueLastUpdated)).setText(DateParserHelper.parseDateToString(content.getLastUpdated()));
        ((TextView)findViewById(R.id.valueDescription)).setText(content.getDescription());
        //We initialize the button
        initButton();
    }
    
    void initButton() {
        Button btnReadContent = (Button)findViewById(R.id.btnReadContent);
        btnReadContent.setVisibility(View.VISIBLE);
        //The button text will depend on content type.
        if(content.getType() != null) {        
	        switch (content.getType()) {
	        case AUDIO:
	        	btnReadContent.setText(R.string.button_read_audio);
	        	break;
	        case IMAGE:
	        	btnReadContent.setText(R.string.button_look_image);
	        	break;
	        case VIDEO:
	        	btnReadContent.setText(R.string.button_look_video);
	        	break;
	        }
        }
        if(content.isSmell()) {
        	//If it's smell, it will be an other text
        	btnReadContent.setText(R.string.button_generate_smell);
        }
    }
     
    public void onReadContentClick(View view) {
    	//When we click on the button, this method is called
    	if(content.getType() != null) {      
	    	String url = getString(R.string.url);
	    	url += content.getFileName();
	    	Uri uri =  Uri.parse(url);
	    	Intent viewMediaIntent = new Intent();   
			viewMediaIntent.setAction(android.content.Intent.ACTION_VIEW);
			String szType = "";
			switch (content.getType()) {
	        case AUDIO:
	        	szType = "audio/*";
	    		//viewMediaIntent.putExtra(Intent.EXTRA_TITLE, content.getTitle());//TODO search how to set title for audio
	        	break;
	        case IMAGE:
	        	szType = "image/*";
	        	break;
	        case VIDEO:
	        	szType = "video/*";
	        	break;
	        }
			//We launch the right player
			viewMediaIntent.setDataAndType(uri, szType);
			viewMediaIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			startActivity(viewMediaIntent);
    	}
	}    


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_content_detail, menu);
        return true;
    }
}
