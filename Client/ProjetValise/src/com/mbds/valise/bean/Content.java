package com.mbds.valise.bean;

import java.io.Serializable;
import java.util.Date;

public class Content implements Serializable {
	private static final long serialVersionUID = 1L;
	private String title;
	private String description;
	private String fileName;
	private Date dateCreated;
	private Date lastUpdated;
	private boolean smell;
	private TagType type;
	public enum TagType { 
		AUDIO, VIDEO, IMAGE 
	};
	
	public Content() {
		this.smell = false;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public Date getDateCreated() {
		return dateCreated;
	}
	
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	public Date getLastUpdated() {
		return lastUpdated;
	}
	
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	
	public boolean isSmell() {
		return smell;
	}
	public void setSmell(boolean smell) {
		this.smell = smell;
	}
	public TagType getType() {
		return type;
	}
	public void setType(String type) {
		this.type = TagType.valueOf(type);
	};
}
