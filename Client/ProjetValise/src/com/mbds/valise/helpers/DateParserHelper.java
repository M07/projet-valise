package com.mbds.valise.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateParserHelper {
	static public String parseDateToString(Date date) {
		if(date != null) {
			DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			return df.format(date);
		}
		return "Unknow";
	}
	
	static public Date parseStringToDate(String date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
	    try {
			return df.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	    return null;
	}
}
