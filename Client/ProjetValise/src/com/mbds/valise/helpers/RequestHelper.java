package com.mbds.valise.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.SocketTimeoutException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import android.util.Log;

import com.mbds.valise.utils.BiblioResponse;
import com.mbds.valise.utils.Constant;

public class RequestHelper {
	static int timeoutConnection = 60000;
	static int timeoutSocket = 60000;

	public static BiblioResponse postRequest( List<NameValuePair> nameValuePairs, String requestUrl) {
		Log.v("Http Post url:", requestUrl);
		BiblioResponse response = new BiblioResponse();
		int code = 400;
		String message =null;
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		HttpPost postMethod = new HttpPost(requestUrl);
		DefaultHttpClient httpClient = new DefaultHttpClient();
		try {
			postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			message = httpClient.execute(postMethod, responseHandler);
			code = Constant.WS_REQUEST_OK;
		} catch (SocketTimeoutException se) {
			//Request Timeout
			code = Constant.WS_REQUEST_TIMEOUT;
			//BiblioResponse.setCode(Constant.AGROTRACKING_REQUEST_TIMEOUT);
			Log.v("Request Timeout", "Time out");
		}catch (UnsupportedEncodingException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			message = e.getMessage();
			Log.v("Request Error", e.getMessage());
		} catch (ClientProtocolException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			message = e.getMessage();
			Log.v("Request Error", e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			message = e.getMessage();
			Log.v("Request Error", e.getMessage());
			code = Constant.WS_NO_NETWORK;
		} catch (Exception e)
		{
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			message = e.getMessage();
			Log.v("Request Error", "Error : " +e.getMessage());
		}
		finally
		{
			response.setCode(code);
			response.setMessage(message);
			Log.v("Request",message);
			closeConnection(httpClient) ;
		}
		return response;
	}

	/**
	 * Execute a http get request
	 *
	 * @param url Url of the request
	 * @return Response the http response
	 * @
	 */
	public static BiblioResponse doGetRequest(String url) {
		Log.v("Http Request Get", url);
		HttpGet get = new HttpGet(url);
		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);
		DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);
		HttpResponse res;
		int code=0;
		String message = null;
		BiblioResponse BiblioResponse = new BiblioResponse();
		try {
			res = httpClient.execute(get);
			code = getResponseCode(res);
			message = getResponse(res);
			Log.v("Message", message);
		} catch (SocketTimeoutException se) {
			//Request Timeout
			code =Constant.WS_REQUEST_TIMEOUT;
			//BiblioResponse.setCode(Constant.AGROTRACKING_REQUEST_TIMEOUT);
			Log.v("Request Timeout", "Time out");
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			Log.v("Error", e.getMessage());
			code= Constant.WS_INTERNAL_ERROR;
			message = "Erreur : " + e.getMessage() ;
		}
		BiblioResponse.setCode(code);
		BiblioResponse.setMessage(message);
		closeConnection(httpClient);
		return BiblioResponse;
	}

	/**
	 * Retrieve the response body from an http response
	 *
	 * @param httpResponse the http response
	 * @return a string
	 */
	public static String getResponse(HttpResponse httpResponse) {
		HttpEntity entity = httpResponse.getEntity();
		String response = null;
		if (entity != null) {
			try {
				InputStream instream = entity.getContent();
				response = convertStreamToString(instream);
				instream.close();
			} catch (IOException e) {
				Log.v("IOException", e.getMessage());
			}
		}
		Log.v("HttpResponse", response);
		return response;
	}

	/**
	 * Close the connection
	 * When we make a request, some ressources are been used; when the request is completely executed
	 * we must free these ressources
	 * @param httpClient
	 */
	public static void closeConnection(DefaultHttpClient httpClient) {
		if (httpClient != null) {
			// Close the connection to free used ressources
			httpClient.getConnectionManager().shutdown();
			httpClient = null;
			Log.v("Http", "Close HttpClient");
		}
	}


	/**
	 * Convert a stream to string
	 *
	 * @param is the input stream
	 * @return the inputstream as string
	 */
	private static String convertStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();
		// Wrap a BufferedReader around the InputStream
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		// Read response until the end
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
			is.close();
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}

		// Return full string
		return total.toString();
	}


	/**
	 * get the code status of an http response
	 *
	 * @param httpResponse the http response
	 * @return the code of the http response
	 */

	public static int getResponseCode(HttpResponse httpResponse) {
		StatusLine status = httpResponse.getStatusLine();
		int code = status.getStatusCode();
		Log.v("Code", "Voici le code " + code);
		//Response Code 2XX : Success
		if (Math.abs(code - 200) < 100)
			code = 200;
		//Response Code 4XX : Client Error
		if (Math.abs(code - 400) < 100)
			code = 400;
		if (Math.abs(code - 500) < 100)
			code = 500;
		//Response Code 5XX : Server Error
		Log.v("Code", "Voici le code " + code);
		return code;
	}

}
