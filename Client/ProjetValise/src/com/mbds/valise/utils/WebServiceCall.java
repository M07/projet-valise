package com.mbds.valise.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.mbds.valise.ContentDetailActivity;
import com.mbds.valise.R;
import com.mbds.valise.bean.Content;
import com.mbds.valise.helpers.DateParserHelper;
import com.mbds.valise.helpers.RequestHelper;


public class WebServiceCall extends AsyncTask<String, Void, String> {

	public static final int REQ_SAVE_STUDENT = 10;

	private String message;
	private Activity activity;
	private int timeoutConnection = 10000;
	private int timeoutSocket = 5000;
	private ProgressDialog mProgressDialog;


	public WebServiceCall(Activity activity) {
		this.activity = activity;
		mProgressDialog = ProgressDialog.show(activity, activity.getText(R.string.title_dialog_progress), activity.getText(R.string.mess_dialog_progress), true);
	}

	@Override
	protected String doInBackground(String... args) {

		HttpRequestBase request = null;
		String tagId  = args[0];
		String url = activity.getString(R.string.url);
		url += activity.getString(R.string.urlWS);
		url += tagId;

		request = new HttpGet(url);

		request.setHeader("Accept", "application/json");
		request.setHeader("Content-type", "application/json");

		HttpParams httpParameters = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
		HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

		DefaultHttpClient  httpClient = new DefaultHttpClient(httpParameters);

		HttpResponse response;

		try {
			response = httpClient.execute(request);
			Log.d("WebInvoke", "Saving : " + response.getStatusLine().getStatusCode());
			message  = RequestHelper.getResponse(response);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			httpClient.getConnectionManager().shutdown();
		}

		return args[0];
	}

	/**
	 * Retrieve the response body from an http response
	 *
	 * @param httpResponse the http response
	 * @return a string
	 */
	public  String getResponse(HttpResponse httpResponse) {
		HttpEntity entity = httpResponse.getEntity();
		String response = null;
		if (entity != null) {
			try {
				InputStream instream = entity.getContent();
				response = convertStreamToString(instream);
				instream.close();
			} catch (IOException e) {
				Log.i("IOException", e.getMessage());
			}
		}
		Log.i("HttpResponse", response);
		return response;
	}
	/**
	 * Convert a stream to string
	 *
	 * @param is the input stream
	 * @return the input stream as string
	 */
	private  String convertStreamToString(InputStream is) {
		String line = "";
		StringBuilder total = new StringBuilder();
		// Wrap a BufferedReader around the InputStream
		BufferedReader rd = new BufferedReader(new InputStreamReader(is));
		// Read response until the end
		try {
			while ((line = rd.readLine()) != null) {
				total.append(line);
			}
			is.close();
		} catch (IOException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}
		return total.toString();
	}

	// -- gets called just before thread begins
	@Override
	protected void onPreExecute() {
		super.onPreExecute();
		Log.i("makemachine", "onPreExecute()");
	}

	// -- called if the cancel button is pressed
	@Override
	protected void onCancelled() {
		super.onCancelled();
		//if (mustShowLoading)
		Log.i("makemachine", "onCancelled()");
	}

	@SuppressLint("NewApi")
	@Override
	protected void onPostExecute(String action) {
		super.onPostExecute(action);
		Log.i("makemachine", "onPostExecute()");
		if(message != null){
			JSONObject resJO;
			try{
				resJO = new JSONObject(message);
				if(resJO.has("error")) {
					Toast.makeText(activity, resJO.get("error").toString(), Toast.LENGTH_LONG).show();					
				} else {
					Content c = new Content();
					c.setTitle(resJO.getString("title"));
					c.setDescription(resJO.getString("description"));
					c.setDateCreated(DateParserHelper.parseStringToDate(resJO.getString("dateCreated")));
					c.setLastUpdated(DateParserHelper.parseStringToDate(resJO.getString("lastUpdated")));
					if(resJO.has("type")) {						
						c.setFileName(resJO.getString("fileName"));						
						JSONObject typeJO = new JSONObject(resJO.getString("type"));
						if(typeJO.has("name")) {
							c.setType(typeJO.getString("name"));
						}
						
					} else if(resJO.has("smell")) {
						c.setSmell(resJO.getBoolean("smell"));
					}
					Intent myIntent = new Intent(activity, ContentDetailActivity.class);
					myIntent.putExtra("Content", c);
					activity.startActivityForResult(myIntent, 0);
				}
			} catch (JSONException e) {
				e.printStackTrace();
				Toast.makeText(activity, R.string.mess_json_pb , Toast.LENGTH_LONG).show();
			}
		} else {
			Toast.makeText(activity, R.string.mess_http_response_ko, Toast.LENGTH_LONG).show();
		}
		if(mProgressDialog.isShowing()) {
			mProgressDialog.dismiss();
		}
	}
}
