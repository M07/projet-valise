package com.mbds.valise.utils;


public class Constant {
	
	/**
	 *
	 * Web Service Response Code
	 */
	public static final  int WS_REQUEST_OK = 200;
	public static final  int WS_SERVER_ERROR = 999;
	public static final  int WS_INTERNAL_ERROR = 401;
	public static final  int WS_NOT_FOUND = 404;
	public static final  int WS_USER_NOT_AUTHORIZED = 702;
	public static final  int WS_REQUEST_TIMEOUT = 408;
	public static final  int WS_NO_NETWORK = 409;

}
