package com.mbds.valise;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;

import com.mbds.valise.utils.WebServiceCall;

/* 
 * This activity allows users to use NFC reader or use BarCode scanner for QRCode Tag
 * A button exists to launch the QRCode scanner application
 * 
 */
public class HomeActivity extends Activity {
	private static final int REQUEST_SCAN = 0;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
	}

	public void onQRCodeScanClick(View view) {
		Intent intent = new Intent("com.google.zxing.client.android.SCAN");
        intent.putExtra("com.google.zxing.client.android.SCAN_MODE", "QR_CODE_MODE");
		try {
			startActivityForResult(intent, REQUEST_SCAN);
		} catch (ActivityNotFoundException e) {
			//If we didn't find the Barcode Scanner application, we ask to download it (it's free of course)
			e.printStackTrace();
	         new AlertDialog.Builder(this)
	            .setTitle("WARNING:")
	            .setMessage("You don't have Barcode Scanner installed. Please install it.")
	             .setCancelable(false)
	            .setNeutralButton("Install it now", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int whichButton) {         
	                          Uri uri = Uri.parse("market://search?q=pname:com.google.zxing.client.android");
	                          startActivity(new Intent(Intent.ACTION_VIEW, uri));
	                    }
	            })
	            .show();
		}
	}

	public void onActivityResult(int reqCode, int resCode, Intent intent) {		
		if (REQUEST_SCAN == reqCode) {
			if (RESULT_OK == resCode) {
				//If the Barcode Scanner application send us our tag, we send it to web services
				processTag(intent.getStringExtra("SCAN_RESULT"));
			}
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		NfcAdapter mAdapter = NfcAdapter.getDefaultAdapter(this);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		mAdapter.enableForegroundDispatch(this, pendingIntent, null, null);
	}

	void resolveIntent(Intent intent) {
		// Parse the intent and get the action that triggered this intent
		String action = intent.getAction();

		// Check if it was triggered by a tag discovered interruption.
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
			Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			NdefMessage[] messages = null;
			if (rawMsgs != null) {
				messages = new NdefMessage[rawMsgs.length];
				for (int i = 0; i < rawMsgs.length; i++) {
					messages[i] = (NdefMessage) rawMsgs[i];
				}
				if (messages.length != 0) {
					String message = "";
					for (int i = 0; i < messages.length; i++) {
						NdefRecord record = messages[0].getRecords()[0];
						message = getTextData(record.getPayload());
					}
					processTag(message);
				}
			}
		}
	}

	private String getTextData(byte[] data) {

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			String bs = Integer.toHexString(data[i] & 0xFF);
			if (bs.length() == 1) {
				sb.append(0);
			}
			sb.append(bs);
		}
		return sb.toString();
	}

	@Override
	public void onNewIntent(Intent intent) {
		setIntent(intent);
		resolveIntent(intent);
	}

	protected void processTag(String message) {
		//We call our web service
		new WebServiceCall(this).execute(message);
	}
}
